#!/usr/bin/env bash
# make curl use ipv4 all the time (with -4) because sometimes DNS resolution failed
# disabled ipv6 with http://askubuntu.com/a/484487/560590
LOG=$(curl --ipv4 --max-time 10 'https://api.redsmin.com/auth/login' --data-binary '{"email":"jenkins-check@fgribreau.com","password":"PASSWORD"}')
IS_API_UP=$(echo $LOG | grep -i "invalid email")
[[ -z $IS_API_UP ]] && echo "❌  API (mongodb connection ?) down" && echo $LOG && exit 1
echo "👍  Everything's good"
